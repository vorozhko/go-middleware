package logger

import (
	"context"
	"log"
	"net/http"
	"os"
	"time"
)

type loggerContextType int

const LoggerContextKey loggerContextType = 1

type Logger struct {
	*log.Logger
}

//initLogger - wrap stdlib log package into Logger
func InitLogger() *Logger {
	return &Logger{log.New(os.Stdout, "logger: ", log.Lmicroseconds)}
}

//Log - provide http Handler ServeHTTP for Logger
func (this *Logger) Log(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), LoggerContextKey, this)
		t1 := time.Now()
		h.ServeHTTP(w, r.WithContext(ctx))
		t2 := time.Now()
		this.Printf("%s %q %v\n", r.Method, r.URL.String(), t2.Sub(t1))
	})
}
