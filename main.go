package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/vorozhko/middleware/logger"
	"gitlab.com/vorozhko/middleware/recovery"
	"gitlab.com/vorozhko/middleware/securehttp"

	"github.com/justinas/alice"
	"github.com/justinas/nosurf"
	"github.com/throttled/throttled"
	"github.com/throttled/throttled/store/memstore"
)

func main() {
	//Logging middleware
	myLog := logger.InitLogger()

	//rate limit middleware
	httpRateLimiter := initHttpRateLimit(20, 5)

	//security headers middleware
	security := securehttp.InitSecurityHTTPOptions()
	security.SetHSTS("max-age=31536000; includeSubDomains")
	security.SetXFrameOptions("sameorigin")

	//hello world app handler
	myAppHandler := http.HandlerFunc(myApp)

	//init a chain of middleware
	chain := alice.New(security.SecureHTTP, nosurf.NewPure, httpRateLimiter.RateLimit, myLog.Log, recovery.Recover).Then(myAppHandler)
	http.ListenAndServe(":8080", chain)
}

//myApp - hello world app.
//Add query parametr p to test for Application recover from panic
func myApp(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("p") != "" {
		w.WriteHeader(502)
		fmt.Println("Panicking!")
		panic("tests panic")
	}
	w.WriteHeader(200)
	w.Write([]byte("Hello world!"))
	time.Sleep(1 * time.Second)
}

//initHttpRateLimit - init throttled.HTTPRateLimiter object
func initHttpRateLimit(maxRatePerMinute int, maxBurstPerRate int) throttled.HTTPRateLimiter {
	store, err := memstore.New(65536)
	if err != nil {
		log.Fatal(err)
	}

	quota := throttled.RateQuota{MaxRate: throttled.PerMin(maxRatePerMinute), MaxBurst: maxBurstPerRate}
	rateLimiter, err := throttled.NewGCRARateLimiter(store, quota)
	if err != nil {
		log.Fatal(err)
	}

	return throttled.HTTPRateLimiter{
		RateLimiter: rateLimiter,
		VaryBy:      &throttled.VaryBy{Path: true},
	}
}
