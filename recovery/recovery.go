package recovery

import (
	"net/http"

	"gitlab.com/vorozhko/middleware/logger"
)

//Recovery middleware

//myRecover - setup recover for http
func Recover(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				mylog := r.Context().Value(logger.LoggerContextKey).(*logger.Logger)
				mylog.Printf("panic: %+v", err)
				http.Error(w, "500 - Internal Server Error", http.StatusInternalServerError)
			}
		}()

		h.ServeHTTP(w, r)
	})
}
