package securehttp

import "net/http"

// HTTP security headers middleware

type SecurityHTTPOptions struct {
	headers map[string]string
}

//InitSecurityHTTPOptions - provide SecurityHTTPOptions object and init headers map
func InitSecurityHTTPOptions() *SecurityHTTPOptions {
	var security SecurityHTTPOptions
	security.headers = make(map[string]string, 1)
	return &security
}

//SecureHTTP - setup HTTP security headers middleware
func (security *SecurityHTTPOptions) SecureHTTP(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		for header, value := range security.headers {
			w.Header().Add(header, value)
		}

		h.ServeHTTP(w, r)
	})
}

//SetXFrameOptions - set X-Frame-Options header
//
//deny - The page cannot be displayed in a frame, regardless of the site attempting to do so.
//
//sameorigin - The page can only be displayed in a frame on the same origin as the page itself.
//
//See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
func (security *SecurityHTTPOptions) SetXFrameOptions(v string) {
	security.headers["X-Frame-Options"] = v
}

//SetHSTS - set HTTP Strict-Transport-Security, known as HSTS
//
//max-age=<expire-time> - The time, in seconds, that the browser should remember that a site is only to be accessed using HTTPS.
//
//includeSubDomains Optional - If this optional parameter is specified, this rule applies to all of the site's subdomains as well.
//
//preload Optional - See Preloading Strict Transport Security for details. Not part of the specification.
//
//See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security
func (security *SecurityHTTPOptions) SetHSTS(v string) {
	security.headers["Strict-Transport-Security"] = v
}
