# Go middleware for http web server

Go middleware project demonstrate:
* How to chain functions in Go
* How to define middleware functions
* How middleware components communicate
* How to customize middleware components

## How to chain functions in Go
To chain functions like middleware [Alice](https://github.com/justinas/alice) was choosen.

The only requirements that all middleware constructors should have form of:
```
func (http.Handler) http.Handler
```

## How to define middleware functions

Lets review following example of Logger.Log function:
```
func (this *Logger) Log(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), LoggerContextKey, this)
		t1 := time.Now()
		h.ServeHTTP(w, r.WithContext(ctx))
		t2 := time.Now()
		this.Printf("%s %q %v\n", r.Method, r.URL.String(), t2.Sub(t1))
	})
}
```
* The definition is ```func (this *Logger) Log(h http.Handler) http.Handler``` as required by Alice framework
* It receive http.ResponseWriter and http.Request objects to build log string
* It is using context package to pass Logger object to other middleware functions

## How middleware components communicate

Using go stdlib context package we can pass object between middleware functions with ```http.Request.WithContext```, ```context.WithValue``` and ```http.Request.Context().Value()```.
In next example logger.Log function create context with Logger object and pass it to http.Request object
```
func (this *Logger) Log(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), LoggerContextKey, this)
        h.ServeHTTP(w, r.WithContext(ctx))
	})
}
```

Now Logger object is available from http.Request object in other middleware functions like in recovery.Recover:
```
func Recover(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				mylog := r.Context().Value(logger.LoggerContextKey).(*logger.Logger)
				mylog.Printf("panic: %+v", err)
				http.Error(w, "500 - Internal Server Error", http.StatusInternalServerError)
			}
		}()

		h.ServeHTTP(w, r)
	})
}
```

One important port that logger.Log have to be initialized before recoery.Recover. To achieve that we need to chain them appropriately.
 
## How to customize middleware components

Some components needs to be initialized before use. For example to define output stream for logs we need to do additional steps with log object. 

To achieve that we can define custom Logger type which will do two things:
* provide ```func (*Logger) Log(h http.Handler) http.Handler``` for Alice framework
* provide special init function as a constructor
```
func InitLogger() *Logger {
	return &Logger{log.New(os.Stdout, "logger: ", log.Lmicroseconds)}
}

func (this *Logger) Log(h http.Handler) http.Handler {
    //....
}
```

## Middleware packages in this demo
I wrote following wrappers only __*for demo*__ purposes:
* [Application logs](https://gitlab.com/vorozhko/middleware/logger)
* [Recovering gracefully from panics](https://gitlab.com/vorozhko/middleware/recovery)
* [HTTP security headers: HSTS and X-Frame-Options](https://gitlab.com/vorozhko/middleware/securehttp)

And following ones are very well established and used in many go projects:
* [CSRF](https://github.com/justinas/nosurf)
* [Requests rate limiting](https://github.com/throttled/throttled)